<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Alteração de cadastros</title>
  </head>
  <body>
        
    <?php 
        
        include "conexao.php";
        $id = $_GET['id'] ?? '';
        $sql = "SELECT * FROM cadastro_pessoas WHERE id = $id";

        $dados = mysqli_query($conn, $sql);
        $linha = mysqli_fetch_assoc($dados);

     ?>



      <div class="container">
        <div class="row">
          <div class="col">
           <h1>Alteração de cadastro</h1> 
             <form action="edit_cadastro_contatos.php" method="POST">
               <div class="form-group">
                   <label for="nome">Alterar nome</label>
                   <input type="text" class="form-control" placeholder = "Insira o seu nome completo" name="nome" required value="<?php echo $linha['nome'];?>"> 
                 </div>
             <div class="form-group">
                 <label for="email">Alterar e-mail</label>
                 <input type="email" class="form-control" placeholder = "Insira o seu e-mail" name="email" required value="<?php echo $linha['email'];?>"> 
               </div>
               <div class="form-group">
                   <label for="senha">Alterar senha</label>
                   <input type="text" class="form-control" placeholder = "Insira a sua senha" name="senha" required value="<?php echo $linha['senha'];?>"> 
                 </div>
                 <div class="form-group">
                     <label for="ativo">Ativo?</label>
                     <input type="text" class="form-control" placeholder = "Sim ou não?" name="ativo" value="<?php echo $linha['ativo'];?>"> 
                   </div>
                   <div class="form-group">
                       <label for="data_criacao">Alterar data de criação</label>
                       <input type="date" class="form-control" placeholder = "Insira a data de criação" name="data_criacao" value="<?php echo $linha['data_criacao'];?>"> 
                     </div>
                     <div class="form-group">
                         <label for="data_atualizacao">Alterar data de atualização</label>
                         <input type="date" class="form-control" placeholder = "Insira a data de atualização" name="data_atualizacao" value="<?php echo $linha['data_atualizacao'];?>"> 
                       </div>
                       <div class="form-group">
                           <input type="submit" class="btn btn-success" value="Salvar Alterações"> 
                           <imput type="hidden" name="id" value="<?php echo $linha['id']?>">
                         </div>
                         <hr>
           </form>
           <a href="index.php" class="btn btn-primary">Voltar para o início</a>
            </div>
          </div>
        </div>
  </body>
</html>