<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Home</title>
  </head>
  <body>
    
      <div class="container">
        <div class="row">
          <div class="col">
           <div class="container-fluid py-5">
        <h1 class="display-5 fw-bold">Cadastro de pessoas</h1>
        <p class="col-md-8 fs-4">Use as opções abaixo para criar um novo cadastro ou editar um cadastro já existente.</p>
        <a class="btn btn-primary" href="form.php" role="button">Cadastrar novo contato</a>
        <a class="btn btn-primary" href="ControllerContatos.php" role="button">Editar cadastros</a>
            </div>
          </div>
        </div>
  </body>
</html>