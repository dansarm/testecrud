<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Cadastro</title>
  </head>
  <body>
    
      <div class="container">
        <div class="row">
          <div class="col">
           <h1>Cadastro</h1> 
             <form action="cadastro_contatos.php" method="POST">
               <div class="form-group">
                   <label for="exampleInputEmail1">Nome completo</label>
                   <input type="text" class="form-control" placeholder = "Insira o seu nome completo" name="nome" required> 
                 </div>
             <div class="form-group">
                 <label for="exampleInputEmail1">Endereço de e-mail</label>
                 <input type="email" class="form-control" placeholder = "Insira o seu e-mail" name="email" required> 
               </div>
               <div class="form-group">
                   <label for="exampleInputEmail1">Criar senha</label>
                   <input type="password" class="form-control" placeholder = "Insira a sua senha" name="senha" required> 
                 </div>
                 <div class="form-group">
                     <label for="exampleInputEmail1">Ativo?</label>
                     <input type="text" class="form-control" placeholder = "Sim ou não?" name="ativo"> 
                   </div>
                   <div class="form-group">
                       <label for="exampleInputEmail1">Data de criação</label>
                       <input type="date" class="form-control" placeholder = "Insira a data de criação" name="data_criacao"> 
                     </div>
                     <div class="form-group">
                         <label for="exampleInputEmail1">Data de atualização</label>
                         <input type="date" class="form-control" placeholder = "Insira a data de atualização" name="data_atualizacao"> 
                       </div>
                       <div class="form-group">
                           <input type="submit" class="btn btn-success"> 
                         </div>
                         <hr>
           </form>
           <a href="index.php" class="btn btn-primary">Voltar para o início</a>
            </div>
          </div>
        </div>
  </body>
</html>