<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Cadastro</title>
  </head>
  <body>
    
    <?php  
      
        $pesquisa = $_POST['busca'] ?? '';
    

      include "conexao.php";

      $sql = "SELECT * FROM cadastro_pessoas WHERE nome LIKE '%$pesquisa%'";
      $dados = mysqli_query($conn, $sql);

      ?>

      <div class="container">
        <div class="row">
          <div class="col">
          <h1>Pesquisar</h1>
          <nav class="navbar navbar-light bg-light">
            <div class="container-fluid">
              <form class="d-flex" action="ControllerContatos.php" method="POST">
                <input class="form-control me-2" type="search" placeholder="Nome do contato" aria-label="Search" name="busca" autofocus>
                <button class="btn btn-outline-success" type="submit" >Pesquisar</button>
              </form>
            </div>
          </nav>
          <div class="bd-example">
            <table class="table table-hover">
                <thead>
              <tr>
                <th scope="col">Nome</th>
                <th scope="col">Email</th>
                <th scope="col">Senha</th>
                <th scope="col">Status</th>
                <th scope="col">Data de criação</th>
                <th scope="col">Data de atualização</th>
                <th scope="col">Opções</th>
              </tr>
            </thead>
            <tbody>
              <?php

               while ($linha = mysqli_fetch_assoc($dados)) {
                  $id = $linha ['id'];
                  $nome = $linha ['nome'];  
                  $email = $linha ['email']; 
                  $senha = $linha ['senha']; 
                  $ativo = $linha ['ativo']; 
                  $data_criacao = $linha ['data_criacao']; 
                  $data_atualizacao = $linha ['data_atualizacao'];
                  $data_criacao = mostra_data($data_criacao);
                  $data_atualizacao = mostra_data($data_atualizacao);
                  

                  echo "<tr>
                            <th scope='row'>$nome</th>
                            <td>$email</td>
                            <td>$senha</td>
                            <td>$ativo</td>
                            <td>$data_criacao</td>
                            <td>$data_atualizacao</td>
                            <td width=100px>
                                <a href='edit_cadastro.php?id=$id' class='btn btn-success btn-sm'>Editar</a>
                                <a href='#' class='btn btn-danger btn-sm' data-toggle='modal' data-target='#confirma' onclick=" .'"' ."pegar_dados($id, '$nome')" .'"' .">Excluir</a>

                            </td>
                        </tr>";
               }


              ?>
              
            </tbody>

            </table>
            <a class="btn btn-info" href="index.php">Voltar para o início</a>
          </div>
          </div>
        </div>
       
        <!-- Modal -->
       
        <div class="modal fade" id="confirma" tabindex="-1" role="dialog"aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Confirmação de exclusão</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="excluir_contatos.php" method="POST">
               <p>Deseja realmente excluir <b  id="nome_pessoa">Nome da pessoa</b>?</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button>
                <input type="hidden" name="nome" id="nome_pessoa_1" value=" ">
                <input type="hidden" name="id" id="cod_contato" value=" ">
                <input type="submit" class="btn btn-danger" value="Sim, excluir contato!">
              </form>
              </div>
            </div>
          </div>
        </div>

        <script type="text/javascript">
          
          function pegar_dados(id, nome) {

            document.getElementById('nome_pessoa').innerHTML = nome;
             document.getElementById('nome_pessoa_1').value = nome;
            document.getElementById('cod_contato').value = id;
          }

        </script>
  </body>
</html>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>