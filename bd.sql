CREATE DATABASE crudsql;

CREATE TABLE cadastro_pessoas 
( id int unsigned auto_increment primary key,
  nome varchar(80) not null,
  email varchar(50) default null,
  senha varchar(80) default null,
  data_criacao date, 
  data_atualizacao date
);